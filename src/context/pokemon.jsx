import React, { createContext, useState, useContext } from 'react';
import PropTypes from 'prop-types';

const PokemonContext = createContext();

export default function PokemonProvider({ children }) {
  const [pokemon, setPokemon] = useState();
  const [pokedex, setPokedex] = useState([]);

  return (
    <PokemonContext.Provider
      value={{
        pokemon, setPokemon, pokedex, setPokedex,
      }}
    >
      {children}
    </PokemonContext.Provider>
  );
}

PokemonProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export function usePokemon() {
  const context = useContext(PokemonContext);
  const {
    pokemon, setPokemon, pokedex, setPokedex,
  } = context;
  return {
    pokemon, setPokemon, pokedex, setPokedex,
  };
}
