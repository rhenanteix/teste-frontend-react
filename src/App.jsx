import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';

import HomePage from './screens/Home/HomePage';
import MapPage from './screens/Map/MapPage';

import PokemonProvider from './context/pokemon';

const App = () => (
  <Switch>
    <PokemonProvider>
      <Route path="/home" component={HomePage} />
      <Route path="/map" component={MapPage} />
      <Redirect to="/home" />
    </PokemonProvider>
  </Switch>
);

export default App;
