import React from 'react';
import { Link } from 'react-router-dom';

import pokemonLogo from '../../assets/images/pokemonLogo.png';

const HomePage = () => (
  <div className="home">
    <img src={pokemonLogo} alt="Pokemon" />
    <Link to="/map">
      Start
    </Link>
  </div>
);

export default HomePage;
