import React, { useState } from 'react';
import api from '../../api/api';

import Modal from '../../components/Modal';
import Card from '../../components/Card';
import Form from '../../components/Form';
import Sidebar from '../../components/Sidebar';

import ashFrontImg from '../../assets/images/ashFront.png';
import loadingTooltipImg from '../../assets/images/loadingTooltip.png';

import { usePokemon } from '../../context/pokemon';

const MapPage = () => {
  const { pokemon, setPokemon } = usePokemon();
  const [modalCard, setModalCard] = useState(false);
  const [modalForm, setModalForm] = useState(false);
  const [loading, setLoading] = useState(false);

  const randomPokemon = async () => {
    setLoading(true);
    try {
      const random = Math.floor(Math.random() * (806 - 0) + 0);
      const response = await api.get(`/pokemon/${random}`);

      setPokemon(response.data);
    } catch (err) {
      // Erro
    } finally {
      setLoading(false);
      setModalCard(true);
    }
  };

  return (
    <div className="map">

      <Modal show={modalCard} setShow={setModalCard}>
        { pokemon && (
          <Card />
        )}
      </Modal>

      <Modal show={modalForm} setShow={setModalForm}>
        <Form />
      </Modal>

      <button
        type="button"
        className="__btnAsh"
        onClick={() => randomPokemon()}
      >
        {loading && (
          <img src={loadingTooltipImg} alt="Loading" className="__loading" />
        )}

        <img src={ashFrontImg} alt="Ash" />
      </button>

      <Sidebar showModal={{
        card: setModalCard,
        form: setModalForm,
      }}
      />
    </div>
  );
};

export default MapPage;
