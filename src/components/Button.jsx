import React from 'react';
import PropTypes from 'prop-types';

const Button = ({ title, ...rest }) => <button type="button" className="button" {...rest}>{ title }</button>;

Button.propTypes = {
  title: PropTypes.string.isRequired,
};

export default Button;
