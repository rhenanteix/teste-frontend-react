import React from 'react';
import PropTypes from 'prop-types';

import { usePokemon } from '../context/pokemon';

const Sidebar = ({ showModal }) => {
  const { pokedex, setPokemon } = usePokemon();
  const spaces = [];

  const viewPokemon = (pokemon) => {
    setPokemon(pokemon);
    showModal.card(true);
  };

  for (let i = 0; i < 6; i++) {
    if (pokedex[i]) {
      spaces.push(
        <div
          key={i}
          className="sidebar__item __pokemon"
          onClick={() => viewPokemon(pokedex[i])}
        >
          <img src={pokedex[i].sprites.front_default} alt={pokedex[i].name} />
        </div>,
      );
    } else {
      spaces.push(
        <div key={i} className="sidebar__item __empty">
          ?
        </div>,
      );
    }
  }

  return (
    <div className="sidebar">
      { spaces }
      <div
        className="sidebar__item __create"
        onClick={() => showModal.form(true)}
      >
        +
      </div>
    </div>
  );
};

Sidebar.propTypes = {
  showModal: PropTypes.func.isRequired,
};

export default Sidebar;
