import React from 'react';

import pokeballImg from '../assets/images/pokeball.png';

import Button from './Button';

import { usePokemon } from '../context/pokemon';

const Card = () => {
  const { pokemon, pokedex, setPokedex } = usePokemon();
  const height = pokemon.height / 10;
  const weight = pokemon.weight / 10;
  const hp = pokemon.stats.find((item) => item.stat.name === 'hp');
  const pokedexFind = pokedex?.find((item) => item.id === pokemon.id);
  const stats = pokemon.stats.filter((item) => item.stat.name !== 'hp');

  const abilities = pokemon.abilities.reduce((accumulator, index) => `${index.ability.name}, ${accumulator}`, '')
    .replace(/,\s*$/, '');

  const captured = () => {
    setPokedex([...pokedex, pokemon]);
  };

  const liberatePokedex = (pokemonId) => {
    setPokedex(pokedex.filter((item) => item.id !== pokemonId));
  };

  return (
    <div className="card">

      <div className="__slot">
        <img src={pokemon.sprites.front_default} alt="" />
      </div>

      <div className="__content">

        <h1>{ pokemon.name }</h1>

        <article className="__status">
          <article>
            <span>HP</span>
            <strong>
              {hp.base_stat}
              /
              {hp.base_stat}
            </strong>
          </article>

          <div />

          <article>
            <span>Altura</span>
            <strong>
              {height}
              m
            </strong>
          </article>

          <div />

          <article>
            <span>Peso</span>
            <strong>
              {weight}
              kg
            </strong>
          </article>
        </article>

        <fieldset>
          <legend>Tipo</legend>
        </fieldset>

        <article className="__type">
          {pokemon.types.map((item) => (
            <span
              key={item.type.name}
              className={`type--${item.type.name}`}
            >
              {item.type.name}
            </span>
          ))}
        </article>

        <fieldset>
          <legend>Habilidades</legend>
        </fieldset>

        <article className="__skills">
          <span>{abilities}</span>
        </article>

        {pokedexFind && (
          <>
            <fieldset>
              <legend>Estatísticas</legend>
            </fieldset>

            <article className="__stats">
              <ul>
                {stats.map((item) => (
                  <li>
                    <span>
                      <img src={require(`../assets/images/${item.stat.name}-icon.png`)} alt={item.stat.name} />
                      {item.stat.name}
                    </span>
                    <span>
                      {item.base_stat}
                    </span>
                  </li>
                ))}
              </ul>
            </article>
          </>
        )}

        {pokedexFind
          ? (
            <div className="__liberate">
              <Button
                title="Liberar Pokemon"
                onClick={() => liberatePokedex(pokemon.id)}
              />
            </div>
          )

          : (
            <button
              type="button"
              className="__pokebola"
              onClick={() => captured()}
            >
              <img src={pokeballImg} alt="Pokebola" />
            </button>
          )}

      </div>
    </div>
  );
};

export default Card;
