import React from 'react';
import PropTypes from 'prop-types';

import closeImg from '../assets/images/close.png';

const Modal = ({ show, setShow, children }) => {
  const showAndHide = show ? 'modal' : 'modal__close';

  return (
    <div className={showAndHide}>
      <div className="modal__content">
        <div className="__container">
          <button
            type="button"
            className="__hide"
            onClick={() => setShow(false)}
          >
            <img src={closeImg} alt="Close" />
          </button>

          {children}

        </div>
      </div>
    </div>
  );
};

Modal.propTypes = {
  show: PropTypes.bool.isRequired,
  setShow: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};

export default Modal;
