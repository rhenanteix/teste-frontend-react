import React from 'react';

import Button from './Button';

import addImageImg from '../assets/images/AddImage.png';
import arrowUpSvg from '../assets/images/arrowUp.svg';
import arrowDownSvg from '../assets/images/arrowDown.svg';

const Form = () => {
  const name = 'form';
  return (
    <div className="form">
      <div className="__slot">
        <img src={addImageImg} alt="" />
      </div>

      <div className="__content">

        <form>

          <div className="__group">
            <label htmlFor="name">Nome</label>
            <input id="name" type="text" placeholder="Nome" name="name" />
          </div>

          <div className="__group">
            <label htmlFor="hp">HP</label>
            <input id="hp" type="number" placeholder="HP" name="hp" />
          </div>

          <div className="__group">
            <label htmlFor="weight">Peso</label>
            <input id="weight" type="number" placeholder="Peso" name="weight" />
            <span>Kg</span>
            <button
              type="button"
              className="__btn __up"
              onClick={() => document.querySelector('#weight').stepUp()}
            >
              <img src={arrowUpSvg} alt="Up" />
            </button>
            <button
              type="button"
              className="__btn __down"
              onClick={() => document.querySelector('#weight').stepDown()}
            >
              <img src={arrowDownSvg} alt="Down" />
            </button>
          </div>

          <div className="__group">
            <label htmlFor="height">Altura</label>
            <input id="height" type="number" placeholder="Altura" name="height" />
            <span>Cm</span>
            <button
              type="button"
              className="__btn __up"
              onClick={() => document.querySelector('#height').stepUp()}
            >
              <img src={arrowUpSvg} alt="Up" />
            </button>
            <button
              type="button"
              className="__btn __down"
              onClick={() => document.querySelector('#height').stepDown()}
            >
              <img src={arrowDownSvg} alt="Down" />
            </button>
          </div>

          <fieldset>
            <legend>Tipo</legend>
          </fieldset>

          <div className="__group">
            <input id="types" type="text" placeholder="Nome" name="types" />
          </div>

          <fieldset>
            <legend>Habilidades</legend>
          </fieldset>

          <div className="__group">
            <input id="ability" type="text" placeholder="Habilidade 1" name="ability" />
          </div>

          <div className="__group">
            <input id="ability" type="text" placeholder="Habilidade 2" name="ability" />
          </div>

          <div className="__group">
            <input id="ability" type="text" placeholder="Habilidade 3" name="ability" />
          </div>

          <div className="__group">
            <input id="ability" type="text" placeholder="Habilidade 4" name="ability" />
          </div>

          <fieldset>
            <legend>Estatísticas</legend>
          </fieldset>

          <div className="__submit">
            <Button title="Criar Pokemon" type="submit" />
          </div>

        </form>

      </div>
    </div>
  );
};

export default Form;
